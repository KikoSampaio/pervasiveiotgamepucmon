# README #

A pervasive game using ContextNet middleware from LAC (Laboratory for Advanced Collaboration). 
This is the result of a collaboration between LAC and ICAD/VisionLab (Laboratory of Visualization, Digital TV/Cinema, and Games) from PUC-Rio.

This repository is shared with the goal of aiding future similar projects. For a guide on how to setup the environment
with the components used in this project (ContextNet, LibGDX, AndroidStudio...) please refer to the Guide.pdf file in the
download section of this repository, or download directly via this link: [Guide](https://bitbucket.org/KikoSampaio/pervasiveiotgamepucmon/downloads/Guide.pdf)